#!/bin/bash

IMG_NAME="nickodema/hsl_2022"

if [ -n "$(which nvidia-smi)" ] && [ -n "$(nvidia-smi)" ]; then
    IMG_NAME="${IMG_NAME}:nvidia_updated"
else
    IMG_NAME="${IMG_NAME}:general_updated"
fi

docker commit hsl_2022  ${IMG_NAME} > /dev/null