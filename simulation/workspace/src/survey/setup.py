import os
from glob import glob
from setuptools import setup

package_name = 'nav2_bringup'

setup(

    # Other parameters ...

    data_files=[
        # ... Other data files
        # Include all launch files.
        (os.path.join('share', package_name, 'bringup_launch')),
    ]
)