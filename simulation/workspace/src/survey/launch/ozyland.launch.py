#!/usr/bin/env python3

# Written by Nikolay Dema <ndema2301@gmail.com>, September 2022

import os

from launch import LaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution, LaunchConfiguration
from launch.actions import LogInfo, DeclareLaunchArgument, ExecuteProcess, IncludeLaunchDescription

from launch_ros.substitutions import FindPackageShare
from launch_ros.actions import Node

from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    bringup_dir = get_package_share_directory('nav2_bringup')

    pkg_path = "/workspace/src/survey"

    config_path = pkg_path + "/config"
    map_path = pkg_path + "/map"
    world_path = pkg_path + "/worlds/ozyland.world"

    os.environ["GAZEBO_MODEL_PATH"] = pkg_path + "/models"


    return LaunchDescription([

        ExecuteProcess(
            output = "screen",
            cmd    = ["gazebo",
                        "--verbose",
                        "-s", "libgazebo_ros_init.so",
                        world_path]
        ),

        Node(
            package    = "tf2_ros",
            executable = "static_transform_publisher",
            arguments  = ["0", "0", "0.775", "0", "0", "0", "base_link", "lidar"],
            output     = "screen"
        ),

        Node(
            package    = "tf2_ros",
            executable = "static_transform_publisher",
            arguments  = ["0.42", "0", "1.75", "-0.5", "0.5", "-0.5", "0.5", "base_link", "camera"],
            output     = "screen"
        ),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('nav2_bringup'),
                    'launch',
                    'bringup_launch.py'
                ])
            ]),
            launch_arguments={
                'slam':'True',
                'map': map_path + 'ozyland_map_description.yaml',
                'use_sim_time':'True',
                'params_file': config_path + '/nav2_params.yaml',
                'slam_params_file': config_path + '/slam_params_online_async.yaml'
            }.items()
        ),

        Node(
            package="rviz2",
            executable="rviz2",
            name="rviz2",
            output="screen",
            arguments=['-d' + config_path + '/config.rviz']
        )

    ])
