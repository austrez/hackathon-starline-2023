import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge
from pyzbar import pyzbar
from std_msgs.msg import String


detected_codes = set()


class ImageSubscriber(Node):

  def __init__(self): 
    super().__init__('image_subscriber')
    self.codes_publisher = self.create_publisher(String, '/detected_codes', 10)
    self.period = 1
    self.timer = self.create_timer(self.period, self.timer_callback)
    self.subscriber = self.create_subscription(
      Image, 
      '/camera/image_raw', 
      self.listener_callback, 
      10)
    self.br = CvBridge()
    #self.path = "//workspace//src//qr_detect//detected//"
    #my_file = open(self.path, "w")
   
  def timer_callback(self):
    msg = String()
    msg.data = str(sorted(list(detected_codes)))
    self.codes_publisher.publish(msg)

  def listener_callback(self, data):
    current_frame = self.br.imgmsg_to_cv2(data)
    gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
    barcodes = pyzbar.decode(gray)
    for barcode in barcodes:
        barcode_info = barcode.data.decode('utf-8')
        detected_codes.add(barcode_info)

def main(args=None):
  rclpy.init(args=args) 
  image_subscriber = ImageSubscriber()
  rclpy.spin(image_subscriber)
  image_subscriber.destroy_node() 
  rclpy.shutdown()

  
if __name__ == '__main__':
  main()
